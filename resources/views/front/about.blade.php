@extends('front.layout.master')
@section('force-css','index-1')

@section('style')

@stop
@section('content')
    @include('front.layout.header')
    <!-- =========== nav end =========== -->
    <section id="content" class="ftco-section contact-section ftco-degree-bg">
             <div class="container">
                 <div id="main">
                     <div class="image-style style1 large-block">


                         <h1 class="title">{{__($page_title)}}</h1>
                         <p>{!! $basic->about !!}</p>

                         <div class="clearfix"></div>
                     </div>

                 </div> <!-- end main -->
             </div>
         </section>
    @include('front.layout.footer')
@stop


@section('script')
     <!-- <script type="text/javascript" src="{{asset('')}}assets/frontend/js/jquery-1.11.1.min.js"></script> -->
    <!-- <script type="text/javascript">var jquery_2_2 = $.noConflict(true);</script> -->
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

@stop
@section('js')
<script type="text/javascript">
  $("#datetimepicker").flatpickr({
      minDate: "today",
      maxDate: new Date().fp_incr(50), // 14 days from now
      dateFormat: "d M Y",
  });

</script>
@stop
