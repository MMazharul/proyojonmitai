@extends('front.layout.master')
@section('force-css','index-1')

@section('style')

@stop
@section('content')
    @include('front.layout.header')


    <!-- =========== Main Ticket Booking Area Start ===================== -->
  <section id="breadcrumb" class="ftco-section2 testimony-section bg-light">
      <div id="ticket-booking">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>@lang('Operator')</td>
                                <td>@lang('Departure')</td>
                                <td>@lang('Duration')</td>
                                <td>@lang('Distance')</td>
                                <td>@lang('Arrival')</td>
                                <td>@lang('Total Seat')</td>
                                <td>@lang('Fare')</td>
                                <td>@lang('View Seats')</td>
                            </tr>
                            </thead>

                            <tbody>
                            @if(count($checkAssignTrip)>0)
                                @foreach($checkAssignTrip as $data)
                                    <tr>
                                        <td>
                                            <div class="t-box-1">
                                                <h5>{{$data->tripRoute->name}}</h5>
                                                <strong>{{date('d M Y',strtotime($data->start_date))}}</strong>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{date('h:i A',strtotime($data->start_date))}}</h5>
                                            <strong class="text-success">{{$data->tripRoute->start_point_name}}</strong>
                                        </td>
                                        <td>
                                            <div class="media">
                                                <div class="media-body">
                                                    <strong class="text-danger">{{($data->tripRoute->approximate_time) ?? '-'}}</strong>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <strong>{{$data->distance}}</strong>
                                        </td>
                                        <td>
                                            <strong class="text-success">{{$data->tripRoute->end_point_name}}</strong>
                                        </td>

                                        <td>
                                            <div class="p-img">
                                                <p>{{$data->fleetRegistration->total_seat}} seats</p>
                                            </div>
                                            <p class="text-success">{{$data->fleetRegistration->fleetType->name}}</p>
                                        </td>
                                        @php
                                            $ticketPrice =  \App\TicketPrice::where('trip_route_id',$data->trip_route_id)->where('fleet_type_id',$data->fleetRegistration->fleet_type_id)->latest()->first();

                                        @endphp


                                        <td>

                                            <div class="p-img">
                                                @if($ticketPrice)
                                                    <strong>{{($ticketPrice->price) ?? '' }} {{$ticketPrice->currency}}</strong>

                                                @else
                                                    <strong class="text-danger">-</strong>
                                                @endif
                                            </div>
                                        </td>

                                        <td>
                                            <div class="l-box">
                                                <div class="media">
                                                    <div class="media-body align-self-end">
                                                        <div class="link">
                                                            <a href="{{route('view-seat',$data->id)}}" target="_blank">View
                                                                Seats</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">
                                        <h4 class="text-center text-danger margin-top-40 margin-bottom-60">@lang('No result found')!!</h4>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
  </section>
    <!-- =========== Main Ticket Booking Area End ===================== -->
    @include('front.layout.footer')
  @stop

  @section('script')
     <!-- <script type="text/javascript" src="{{asset('')}}assets/frontend/js/jquery-1.11.1.min.js"></script> -->
    <!-- <script type="text/javascript">var jquery_2_2 = $.noConflict(true);</script> -->
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

  @stop
  @section('js')
  <script type="text/javascript">
  $("#datetimepicke4").flatpickr({
      minDate: "today",
      maxDate: new Date().fp_incr(50), // 14 days from now
      dateFormat: "d M Y",
  });

  </script>
  @stop
