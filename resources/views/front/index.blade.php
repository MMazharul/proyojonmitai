@extends('front.layout.master')
@section('force-css','index-1')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">

    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
@stop
@section('content')
    @include('front.layout.header')
    <!-- =========== nav end =========== -->


    <section class="ftco-section services-section bg-light">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-guarantee"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Best Riding Experience</h3>
                <p>We are always ready to give you best service</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-like"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Travellers Love Us</h3>
                <p>Ous first piority is to satisfy ous travellers</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-detective"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Travel With Pleasure</h3>
                <p>We are providing best security to our customers</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services d-block text-center">
              <div class="d-flex justify-content-center"><div class="icon"><span class="flaticon-support"></span></div></div>
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Our Dedicated Support</h3>
                <p>You can contact with us whatever you are</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="ftco-section2 testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-5 heading-section ftco-animate">
            <span class="subheading">Best Bus Company In Bangladesh</span>
            <h2 class=""><strong style="color:#ff2300;">Why</strong> Choose Us?</h2>
            <div class="hr-design2"></div>
            <p>{{$basic->why_us_p}}</p>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-6 heading-section ftco-animate">
            <span class="subheading">Feedback From Our Travellers</span>
            <h2 class=""><strong style="color:#ff2300;">Our</strong> Guests Says</h2>
            <div class="hr-design2"></div>
            <div class="row ftco-animate">
              <div class="col-md-12">
                <div class="carousel-testimony owl-carousel">
                  <div class="item">
                    <div class="testimony-wrap d-flex">
                      <div class="user-img mb-5" style="background-image: url(assets/frontend/images/person_1.jpg)">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="icon-quote-left"></i>
                        </span>
                      </div>
                      <div class="text ml-md-4">
                        <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class="name">Dennis Green</p>
                        <span class="position">Guest from italy</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="testimony-wrap d-flex">
                      <div class="user-img mb-5" style="background-image: url(assets/frontend/images/person_2.jpg)">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="icon-quote-left"></i>
                        </span>
                      </div>
                      <div class="text ml-md-4">
                        <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class="name">Dennis Green</p>
                        <span class="position">Guest from London</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="testimony-wrap d-flex">
                      <div class="user-img mb-5" style="background-image: url(assets/frontend/images/person_3.jpg)">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="icon-quote-left"></i>
                        </span>
                      </div>
                      <div class="text ml-md-4">
                        <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        <p class="name">Dennis Green</p>
                        <span class="position">Guest from Philippines</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="bus-schedule" class="ftco-section2 testimony-section bg-light">
      <div class="container-fluid">
        <div class="row justify-content-start">
          <div class="col-md-12 heading-section ftco-animate">
            <span style="text-align: center;" class="subheading">Best Bus Company In Bangladesh</span>
            <h2 style="text-align: center;font-size: 40px;" class=""><strong style="color:#FF2300;">Bus</strong> Schedule</h2>
            <div class="hr-design"></div>

            <div class="row  content-section">
              <!-- <div class="col-md-1 ftco-animate">
                  <div class="single-slider owl-carousel">
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(assets/frontend/images/order.jpg);"></div>
                    </div>
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(assets/frontend/images/order2.jpg);"></div>
                    </div>
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(assets/frontend/images/order5.png);"></div>
                    </div>
                  </div>
              </div> -->
              <div class="col-md-10 offset-md-1" style="text-align:center;">
                <table class="table table-hover table-responsive" style="max-height:400px;overflow-y:scroll">
                  <thead>
                    <tr>
                      <th scope="col"><h2 style="font-size:20px;">Operator</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Departure</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Duration</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Distance</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Arrival</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Total Seat</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Fare</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">View Seats</h2></th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(count($checkAssignTrip)>0)
                        @foreach($checkAssignTrip as $data)
                        <tr>
                            <td style="width:244px">
                                <div class="t-box-1">
                                    <h6>{{$data->tripRoute->name}}</h6>
                                    <strong>{{date('d M Y',strtotime($data->start_date))}}</strong>
                                </div>
                            </td>
                            <td>
                                <h5>{{date('h:s A',strtotime($data->start_date))}}</h5>
                                <strong class="text-success">{{$data->tripRoute->start_point_name}}</strong>
                            </td>
                            <td>
                                <div class="media">
                                    <div class="media-body">
                                        <strong class="text-danger">{{($data->tripRoute->approximate_time) ?? '-'}}</strong>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <strong>{{$data->distance}}</strong>
                            </td>
                            <td>
                                <strong class="text-success">{{$data->tripRoute->end_point_name}}</strong>
                            </td>

                            <td>
                                <div class="p-img">
                                    <p>{{$data->fleetRegistration->total_seat}}   {{$data->fleetRegistration->fleetType->name}}</p>
                                </div>

                            </td>
                            @php
                                $ticketPrice =  \App\TicketPrice::where('trip_route_id',$data->trip_route_id)->where('fleet_type_id',$data->fleetRegistration->fleet_type_id)->latest()->first();
                            @endphp


                            <td>

                                <div class="p-img">
                                    @if($ticketPrice)
                                        <strong>{{($ticketPrice->price) ?? '' }} {{$basic->currency}}</strong>

                                    @else
                                        <strong class="text-danger">-</strong>
                                    @endif
                                </div>
                            </td>

                            <td>
                                <div class="l-box">
                                    <div class="media">
                                        <div class="media-body align-self-end">
                                            <div class="link">
                                                <a href="{{route('view-seat',$data->id)}}" target="_blank">View
                                                    Seats</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">
                                <h4 class="text-center text-danger margin-top-40 margin-bottom-60">@lang('No result found')!!</h4>
                            </td>
                        </tr>
                    @endif
                    <!-- <tr>
                      <td><h4>########</h4></td>
                      <td><h4>########</h4></td>
                      <td><h4>########</h4></td>
                      <td><h4>########</h4></td>
                      <td><h4>########</h4></td>
                      <td><h4>########</h4></td>
                    </tr> -->

                  </tbody>
                </table>
              </div>
              <!-- <div class="col-md-2">
                  <div class="single-slider owl-carousel">
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(images/order.jpg);"></div>
                    </div>
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(images/order2.jpg);"></div>
                    </div>
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(images/order5.png);"></div>
                    </div>
                  </div>
              </div> -->
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(assets/frontend/images/bg_2.png);">
      <div class="container-fluid">
        <div class="row justify-content-center help-center">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
                <img src="assets/frontend/images/order2.jpg" alt="">
              </div>
              <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
                <h2> Help Center : 012345678</h2>
              </div>
              <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
                <img class="pull-right" src="assets/frontend/images/order5.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="bus-route" class="ftco-section2 testimony-section bg-light">
      <div class="container-fluid">
        <div class="row justify-content-start">
          <div class="col-md-12 heading-section ftco-animate">
            <span style="text-align: center;" class="subheading">Best Bus Company In Bangladesh</span>
            <h2 style="text-align: center;font-size: 40px;" class=""><strong style="color:#ff2300;">Bus</strong> Routes</h2>
            <div class="hr-design"></div>

            <div class="row  content-section">
              <div class="col-md-4">
                <div class="single-slider owl-carousel">
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(assets/frontend/images/order.jpg);"></div>
                    </div>
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(assets/frontend/images/order2.jpg);"></div>
                    </div>
                    <div class="item">
                      <div class="hotel-img" style="background-image: url(assets/frontend/images/order5.png);"></div>
                    </div>
                  </div>
              </div>
              <div class="col-md-8" style="text-align:center;">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th scope="col"><h2 style="font-size:20px;">From</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">To</h2></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td><h4>Dhaka</h4></td>
                      <td><h4>Chattogram , Coxsbazar , Sylhet</h4></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><h4>Chattogram</h4></td>
                      <td><h4>Coxsbazar , Dhaka , Sylhet </h4></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><h4>Coxsbazar</h4></td>
                      <td><h4>Chattogram , Dhaka , Sylhet </h4></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><h4>Sylhet</h4></td>
                      <td><h4>Chattogram , Dhaka , Chattogram</h4></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(assets/frontend/images/bg_1.png);">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                  <div class="text">

                    <strong class="number" data-number="{{$total_customer}}">0</strong>

                    <span>Total Customers</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                  <div class="text">
                    <strong class="number" data-number="{{$total_bus}}">0</strong>
                    <span>Total Bus</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                  <div class="text">
                    <strong class="number" data-number="{{$total_staff}}">0</strong>
                    <span>Total Stuff</span>
                  </div>
                </div>
              </div>
              <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                  <div class="text">
                    <strong class="number" data-number="{{$total_counter}}">0</strong>
                    <span>Total Counter</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="bus-counter" class="ftco-section2 testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-12 heading-section ftco-animate">
            <span style="text-align: center;" class="subheading">Best Bus Company In Bangladesh</span>
            <h2 style="text-align: center;font-size: 40px;" class=""><strong style="color:#ff2300;">Bus</strong> Counters</h2>
            <div class="hr-design"></div>

            <div class="row  content-section">
              <div class="col-md-10 offset-md-2" style="text-align:center;">
                <table class="table table-hover table-responsive" style="max-height:400px;overflow-y:scroll">
                  <thead>
                    <tr>
                      <th></th>
                      <th scope="col"><h2 style="font-size:20px;">Counter Name</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Address</h2></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($counters as $counter)

                    <tr>
                      <td></td>
                      <td><h4>{{$counter->counter_name}}</h4></td>
                      <td><h4>{{$counter->address}}</h4></td>
                    </tr>

                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-12 heading-section ftco-animate">
            <span style="text-align: center;" class="subheading">Best Bus Company In Bangladesh</span>
            <h2 style="text-align: center;font-size: 40px;" class=""><strong style="color:#ff2300;">Contact</strong> Informations</h2>
            <div class="hr-design"></div>
            <br>
          </div>
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
            <form action="#">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Subject">
              </div>
              <div class="form-group">
                <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
              </div>
            </form>

          </div>

          <div class="col-md-6">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d775.6419872996584!2d91.83243958110333!3d22.368883000871975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30ad277fb863e7c7%3A0xca7815554909801c!2sMuradpur%20Bus%20Stop!5e0!3m2!1sen!2sbd!4v1581948487649!5m2!1sen!2sbd" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

          </div>
        </div>
      </div>
    </section>

    @include('front.layout.footer')
@stop


@section('script')
     <!-- <script type="text/javascript" src="{{asset('')}}assets/frontend/js/jquery-1.11.1.min.js"></script> -->
    <!-- <script type="text/javascript">var jquery_2_2 = $.noConflict(true);</script> -->
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

@stop
@section('js')
<script type="text/javascript">
  $("#datetimepicker2").flatpickr({
      minDate: "today",
      maxDate: new Date().fp_incr(50), // 14 days from now
      dateFormat: "d M Y",
  });

</script>
@stop
