@extends('front.layout.master')
@section('force-css','index-1')

@section('style')

@stop
@section('content')
    @include('front.layout.header')
    <!-- =========== nav end =========== -->
    <section id="bus-counter" class="ftco-section2 testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-12 heading-section ftco-animate">
            <span style="text-align: center;" class="subheading">Best Bus Company In Bangladesh</span>
            <h2 style="text-align: center;font-size: 40px;" class=""><strong style="color:#ff2300;">Bus</strong> Counters</h2>
            <div class="hr-design"></div>

            <div class="row  content-section counter-section" >
              <div class="col-md-10 offset-md-2">
                <table class="table table-hover table-responsive" style="max-height:800px;overflow-y:scroll">
                  <thead>
                    <tr>
                      <th></th>
                      <th scope="col" style="width:40%"><h2 style="font-size:20px;">Counter Name</h2></th>
                      <th scope="col"><h2 style="font-size:20px;">Address</h2></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($counters as $counter)

                    <tr>
                      <td></td>
                      <td><h4>{{$counter->counter_name}}</h4></td>
                      <td><h4>{{$counter->address}}</h4></td>
                    </tr>

                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>

    @include('front.layout.footer')
@stop


@section('script')
     <!-- <script type="text/javascript" src="{{asset('')}}assets/frontend/js/jquery-1.11.1.min.js"></script> -->
    <!-- <script type="text/javascript">var jquery_2_2 = $.noConflict(true);</script> -->
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

@stop
@section('js')
<script type="text/javascript">
  $("#datetimepicker1").flatpickr({
      minDate: "today",
      maxDate: new Date().fp_incr(50), // 14 days from now
      dateFormat: "d M Y",
  });

</script>
@stop
