<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="zxx">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title> @isset($page_title)  {{__($page_title)}} | @endisset  {{__($basic->sitename)}}  </title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"/>

    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/animate.css">

    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/magnific-popup.css">

    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/aos.css">

    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/ionicons.min.css">

    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/jquery.timepicker.css">


    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/')}}/css/responsive.css">
    <style>
      .hotel-img {
      height: 335px !important;
      margin-bottom: 0em !important;
    }
      .owl-carousel .owl-nav {
        display: none !important;
      }
      .owl-carousel .owl-dots {
      display: none !important;
    }
    @media screen and (max-width: 479px) {
     .heading .ftco-animate{
        margin-top:40px !important;
    }
    }
    .counter-section h4,.counter-section h2{
      text-align:left !important;
    }
    </style>

    @yield('style')

</head>

<body class="@yield('body_class')">


    @yield('content')



  <!-- loader -->
   <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> 



  <script src="{{asset('assets/frontend')}}/js/jquery.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/popper.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/bootstrap.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery.easing.1.3.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery.waypoints.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery.stellar.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/owl.carousel.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery.magnific-popup.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/aos.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery.animateNumber.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/bootstrap-datepicker.js"></script>
  <script src="{{asset('assets/frontend')}}/js/jquery.timepicker.min.js"></script>
  <script src="{{asset('assets/frontend')}}/js/scrollax.min.js"></script>
  <script src="{{asset('assets/frontend')}}/https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{asset('assets/frontend')}}/js/google-map.js"></script>
  <script src="{{asset('assets/frontend')}}/js/main.js"></script>

@yield('script')


@yield('js')
@if (session('success'))
    <script>
        $(document).ready(function () {
            swal("Success!", "{{ session('success') }}", "success");
        });
    </script>
@endif

@if (session('alert'))
    <script>
        $(document).ready(function () {
            swal("Sorry!", "{{ session('alert') }}", "error");
        });
    </script>
@endif

@if(Session::has('message'))
    <script>
        var type = "{{Session::get('alert-type','info')}}";
        switch (type) {
            case 'info':
                toastr.info("{{Session::get('message')}}");
                break;
            case 'warning':
                toastr.warning("{{Session::get('message')}}");
                break;
            case 'success':
                toastr.success("{{Session::get('message')}}");
                break;
            case 'error':
                toastr.error("{{Session::get('message')}}");
                break;
        }
    </script>
@endif

</body>

</html>
