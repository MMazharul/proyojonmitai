<footer class="ftco-footer ftco-bg-dark ftco-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <img src="{{asset('assets/frontend')}}/images/logo.png" style="width:100%;">
          <p>Shadhin Travels Ltd. is one of the upcoming bus company in bangladesh. We provide exclusive transport service. Take a look nearest our counter. Book your ticket . . .</p>
          <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4 ml-md-5">
          <h2 class="ftco-heading-2">Information</h2>
          <ul class="list-unstyled">
            <li><a href="#" class="py-2 d-block">Admin</a></li>
            <li><a href="#" class="py-2 d-block">Gallery</a></li>
            <li><a href="#" class="py-2 d-block">Online Sign-in</a></li>
            <li><a href="#" class="py-2 d-block">Create An Account</a></li>
            <li><a href="#" class="py-2 d-block">Cancel Ticket</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
         <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Customer Support</h2>
          <ul class="list-unstyled">
            <li><a href="#" class="py-2 d-block">FAQ</a></li>
            <li><a href="#" class="py-2 d-block">Payment Option</a></li>
            <li><a href="#" class="py-2 d-block">Booking Tips</a></li>
            <li><a href="#" class="py-2 d-block">How it works</a></li>
            <li><a href="#" class="py-2 d-block">Contact Us</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Have a Questions?</h2>
          <div class="block-23 mb-3">
            <ul>
              <li><span class="icon icon-map-marker"></span><span class="text">{{$basic->address}}</span></li>
              <li><a href="#"><span class="icon icon-phone"></span><span class="text">{{$basic->phone}}</span></a></li>
              <li><a href="#"><span class="icon icon-envelope"></span><span class="text">{{$basic->email}}</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">

        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://www.rcreation-bd.com" target="_blank">R-Creations</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
      </div>
    </div>
  </div>
</footer>
