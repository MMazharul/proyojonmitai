@extends('layout')
@section('force-css','bc blog')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/otherPageResponsive.css')}}">

    @endsection
@section('content')



<!-- =============== Latest News Area Start ============================ -->
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Blog Right Sidebar</h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="#">HOME</a></li>
            <li><a href="#">PAGES</a></li>
            <li class="active">Blog Right Sidebar</li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#"><img src="{{asset('')}}assets/frontend/images/blog/post/image/1.png" alt="" /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">Standard single image post</h1>
                        <div class="post-content">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a neque a tortor tempor in porta sem vulputate. Donec varius felis fermentum nis type specimen book. It has survived not only five centuries.Phasellus vehicula justo eget diam posuere sollicitudin eu tincidunt nulla. Curabitur eleifend  tempor magna, in scelerisque urna placerat vel. Phasellus eget sem id justo consequat egestas quis facilisis metus.Phasellus vehicula justo eget diam posuere sollicitudin eu tincidunt nulla. Curabitur eleifend tempor magna, in scelerisque urna placerat vel. Phasellus eget sem id justo consequat egestas quis facilisis metus.</p>
                            <div class="border-box">
                                <blockquote class="style2"><p>Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit Massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet egeat. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit sodales volutpat sapien varius vel. </p></blockquote>
                            </div>
                            <p>Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan. Nunc a vulputate lectus. Vestibulum eleifend nisl sed massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet eget. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit. Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan. Nunc a vulputate lectus. Vestibulum eleifend nisl sed massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet eget. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit.</p>
                            <p>Massa sagittis vestibulum. Vestibulum pretium blandit tellus, sodales volutpat sapien varius vel. Phasellus tristique cursus erat, a placerat tellus laoreet eget. Fusce vitae dui sit amet lacus rutrum convallis. Vivamus sit amet lectus venenatis est rhoncus interdum a vitae velit.</p>
                        </div>
                        <div class="post-meta">
                            <div class="entry-date">
                                <label class="date">29</label>
                                <label class="month">Aug</label>
                            </div>
                            <div class="entry-author fn">
                                <i class="icon soap-icon-user"></i> Posted By:
                                <a href="#" class="author">Jessica Browen</a>
                            </div>
                            <div class="entry-action">
                                <a href="#" class="button entry-comment btn-small"><i class="soap-icon-comment"></i><span>30 Comments</span></a>
                                <a href="#" class="button btn-small"><i class="soap-icon-wishlist"></i><span>22 Likes</span></a>
                                <span class="entry-tags"><i class="soap-icon-features"></i><span><a href="#">Adventure</a>, <a href="#">Romance</a></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="single-navigation block">
                        <div class="row">
                            <div class="col-xs-6"><a rel="prev" href="#" class="button btn-large prev full-width"><i class="soap-icon-longarrow-left"></i><span>Previous Post</span></a></div>
                            <div class="col-xs-6"><a rel="next" href="#" class="button btn-large next full-width"><span>Next Post</span><i class="soap-icon-longarrow-right"></i></a></div>
                        </div>
                    </div>
                    <div class="about-author block">
                        <h2>About Author</h2>
                        <div class="about-author-container">
                            <div class="about-author-content">
                                <div class="avatar">
                                    <img src="images/shortcodes/author1.png" width="96" height="96" alt="">
                                </div>
                                <div class="description">
                                    <h4><a href="#">Jessica Brown</a></h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                </div>
                            </div>
                            <div class="about-author-meta clearfix">
                                <ul class="social-icons">
                                    <li><a href="#"><i class="soap-icon-twitter"></i></a></li>
                                    <li><a href="#"><i class="soap-icon-googleplus"></i></a></li>
                                    <li><a href="#"><i class="soap-icon-facebook"></i></a></li>
                                    <li><a href="#"><i class="soap-icon-linkedin"></i></a></li>
                                </ul>
                                <a href="#" class="wrote-posts-count"><i class="soap-icon-slider"></i><span><b>30</b> Posts</span></a>
                            </div>
                        </div>

                </div>
            </div>
          
        </div>
    </div>
</section>
<!-- =============== Latest News Area End ============================ -->

@stop
