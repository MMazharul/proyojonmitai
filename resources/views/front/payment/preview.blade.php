@extends('user')
@section('content')
    <section id="pricePlan" class="pricePlan">
        <div class="container">
            <div class="row justify-content-center">


                <div class="col-md-10">
                    @include('errors.alert')
                </div>

                <div class="col-md-8 col-md-offset-2">
                    <div class="card ">
                        <div class="card-header text-center">
                            <h4 class="card-title"><strong>{{ $data->gateway->name }}</strong></h4>
                        </div>
                        <form method="POST" action="{{route('deposit.confirm')}}">
                            {{csrf_field()}}
                            <div class="card-body text-center">

                                <img src="{{asset('assets/images/gateway')}}/{{$data->gateway_id}}.jpg"
                                     style="width: 35%;border-radius: 5px ; margin: 10px 25%;"/>

                                <ul style='font-size: 15px;' class="list-group text-center  nic-color">
                                    <li class="list-group-item"> @lang('Amount') : {{$data->amount}}
                                        <strong>{{$basic->currency}}</strong>
                                    </li>


                                    <li class="list-group-item"> @lang('Charge') :
                                        <strong>{{$data->charge}} </strong>{{ $basic->currency }}</li>
                                    <li class="list-group-item "> @lang('Payable') :
                                        <strong>{{$data->charge + $data->amount}} </strong>{{ $basic->currency }}</li>


                                    <li class="list-group-item"> @lang('In USD') :
                                        <strong>${{$data->usd}}</strong>
                                    </li>


                                </ul>


                            </div>
                            <div class="card-footer">
                                <div class="btn-wrapper">
                                    <button  id="btn-confirm" class=" btn btn-block btn-success btn-lg ">
                                        Pay Now
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>




@stop
@section('js')
    @if($data->gateway_id == 107)
        <form action="{{ route('ipn.paystack') }}" method="POST">
            @csrf
            <script
                    src="//js.paystack.co/v1/inline.js"
                    data-key="{{ $data->gateway->val1 }}"
                    data-email="{{ $data->user->email }}"
                    data-amount="{{ round($data->usd/$data->gateway->val7, 2)*100 }}"
                    data-currency="NGN"
                    data-ref="{{ $data->trx }}"
                    data-custom-button="btn-confirm">
            </script>
        </form>
    @elseif($data->gateway_id == 108)
        <script src="//voguepay.com/js/voguepay.js"></script>
        <script>
            closedFunction = function () {

            }
            successFunction = function (transaction_id) {
                window.location.href = '{{ url('/vogue') }}/' + transaction_id + '/success';
            }
            failedFunction = function (transaction_id) {
                window.location.href = '{{ url('/vogue') }}/' + transaction_id + '/error';
            }

            function pay(item, price) {
                //Initiate voguepay inline payment
                Voguepay.init({
                    v_merchant_id: "{{ $data->gateway->val1 }}",
                    total: price,
                    notify_url: "{{ route('ipn.voguepay') }}",
                    cur: 'USD',
                    merchant_ref: "{{ $data->trx }}",
                    memo: 'Buy ICO',
                    recurrent: true,
                    frequency: 10,
                    developer_code: '5af93ca2913fd',
                    store_id: "{{ $data->user_id }}",
                    custom: "{{ $data->trx }}",

                    closed: closedFunction,
                    success: successFunction,
                    failed: failedFunction
                });
            }

            $(document).ready(function () {
                $(document).on('click', '#btn-confirm', function (e) {
                    e.preventDefault();
                    pay('Buy', {{ $data->usd }});
                });
            })
        </script>

    @endif
@endsection
