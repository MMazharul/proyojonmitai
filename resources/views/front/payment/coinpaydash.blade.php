@extends('layout')
@section('force-css','bc blog blogdetails')

@section('content')
    @include('partials.breadcrumb')



    <section id="pricePlan" class="pricePlan margin-bottom-80 margin-top-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-md-offset-2">
                    <div class="card text-center">
                        <div class="card-body ">
                            <h3> PLEASE SEND EXACTLY <strong class="text-success"> {{ $bcoin }} DASH</strong></h3>
                            <h5>TO <strong class="text-success"> {{ $wallet}}</strong></h5>
                            <br>
                            {!! $qrurl !!}
                        </div>
                        <div class="card-footer">
                            <h5>SCAN TO SEND</h5>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



@endsection
