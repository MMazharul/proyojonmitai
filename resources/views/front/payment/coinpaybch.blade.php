@extends('layout')
@section('force-css','bc blog blogdetails')

@section('content')
    @include('partials.breadcrumb')


    <section id="pricePlan" class="pricePlan margin-top-100 margin-bottom-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-md-offset-2">
                    <div class="card text-center">
                        <div class="card-body ">

                            <h3 class="text-color"> PLEASE SEND EXACTLY <span class="text-success">  {{ $bcoin }}</span> BCH</h3>
                            <h5>TO <span class="text-success">  {{ $wallet}}</span></h5>
                            {!! $qrurl !!}
                        </div>
                        <div class="card-footer">
                            <h5>SCAN TO SEND</h5>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>






@endsection
