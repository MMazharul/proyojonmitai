@extends('front.layout.master')
@section('force-css','index-1')

@section('style')

@stop
@section('content')


    @include('front.layout.header')
    <!-- =========== nav end =========== -->
    <div id="page-wrapper">
      <section id="content" class="ftco-section contact-section ftco-degree-bg">
        <div class="container">
            <!-- <div id="main">

                <div class="text-center yellow-color box" style="font-size: 4em; font-weight: 300; line-height: 1em;">Login!</div>
                <p class="light-blue-color block" style="font-size: 1.3333em;">Please login to your account.</p>
                <div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">
                  @if (session('logout'))
                      <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('logout') }}
                      </div>
                  @endif
                  @if (session('success'))
                      <div class="alert alert-success alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('success') }}
                      </div>
                  @endif
                  @if (session('danger'))
                      <div class="alert alert-danger alert-dismissible">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          {{ session('danger') }}
                      </div>
                  @endif

                    <form class="login-form" id="c-form" action="{{route('login')}}" method="post">
                          @csrf
                        <div class="form-group">
                            <input type="text" name="username" value="{{old('username')}}" class="from-control " placeholder="enter your email or username">
                            @if ($errors->has('username'))
                                <strong class="error" style="color:#cc3300;">{{ $errors->first('username') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="from-control input-text input-large full-width" placeholder="enter your password">
                            @if ($errors->has('password'))
                                <strong class="error" style="color:#cc3300;">{{ $errors->first('password') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-8">
                              <label class="checkbox">
                                  <input type="checkbox" value=""> <a href="{{ route('password.request') }}" class="lostpass">Forgot Password</a>
                              </label>
                            </div>

                            <div class="col-md-4">
                              <label class="checkbox">
                                  <input type="checkbox" value=""><a href="{{ route('register') }}" class="loginwith">Create an Account</a>
                              </label>
                            </div>

                          </div>

                        </div>

                        <button type="submit" class="btn-large full-width sky-blue1">LOGIN TO YOUR ACCOUNT</button>
                    </form>
                </div>
            </div> -->
            <form  id="c-form" action="{{route('login')}}" method="post">
                <div class="text-center yellow-color box" style="font-size: 4em; font-weight: 300; line-height: 1em;">Login!</div>
                <div class="form-group col-6 offset-3">
                  <label for="username">Email address</label>
                  <input type="email" class="form-control" id="username" name="username" placeholder="Enter email">
                  @if ($errors->has('username'))
                      <strong class="error" style="color:#cc3300;">{{ $errors->first('username') }}</strong>
                  @endif
                </div>
                <div class="form-group col-6 offset-3">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" name="password" id="password"  placeholder="Password">
                  @if ($errors->has('password'))
                      <strong class="error" style="color:#cc3300;">{{ $errors->first('password') }}</strong>
                  @endif
                </div>
                <div class="col-6 offset-3">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-8">
                      <a href="{{ route('password.request') }}" class="text-info">Forgot Password</a>
                      </div>

                      <div class="col-md-4">
                        <a href="{{ route('register') }}" class="text-info">Create an Account</a>
                      </div>

                    </div>

                  </div>
                </div>

                <button type="submit" class="btn btn-info col-6 offset-3">Submit</button>

            </form>
        </div>
      </section>


    </div>
    @include('front.layout.footer')
@stop


@section('script')
     <!-- <script type="text/javascript" src="{{asset('')}}assets/frontend/js/jquery-1.11.1.min.js"></script> -->
    <!-- <script type="text/javascript">var jquery_2_2 = $.noConflict(true);</script> -->
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>

@stop
@section('js')
<script type="text/javascript">
  $("#date").flatpickr({
      minDate: "today",
      maxDate: new Date().fp_incr(50), // 14 days from now
      dateFormat: "d M Y",
  });

</script>
@stop
