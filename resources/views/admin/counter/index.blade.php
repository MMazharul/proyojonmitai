@extends('admin.layout.master')
@section('body')
    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <div class="float-left">
                <h2 class="mb-4">{{$page_title}}</h2>
            </div>

            <div class="float-right">
                <a href="{{route('counter.create')}}" class="btn btn-success  btn-icon"> <i
                            class="fa fa-plus"></i> Add New </a>
            </div>
        </div>
        <div class="card-body">
            @include('errors.error')

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Counter Name</th>
                    <th>Location</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>

                @foreach($counters as $k=>$data)
                    <tr>
                        <td data-label="SL">{{++$k}}</td>
                        <td data-label="Counter name"><strong>{{$data->counter_name}}</strong></td>
                        <td data-label="Counter Address"><strong>{{$data->address}}</strong></td>
                        <td data-label="Counter Address"><strong>{{$data->location['name']}}</strong></td>

                        <td data-label="Status">
                            <span class="badge  badge-pill  badge-{{ $data->status ==0 ? 'danger' : 'success' }}">{{ $data->status == 0 ? 'Deactive' : 'Active' }}</span>
                        </td>
                        <td data-label="Action">
                            <a href="{{route('counter.edit',$data->id)}}"
                               class="btn btn-info  btn-icon btn-pill" title="Edit">
                                <i class="fa fa-pencil-alt"></i>
                            </a>
                            <a href="{{route('trip-assign-view',['id_no'=>$data->id_no])}}"
                               class="btn btn-info  btn-icon btn-pill" title="view">
                                <i class="fa fa-eye"></i>
                          </a>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            {!! $counters->render() !!}
        </div>
    </div>

@endsection

@section('script')
@endsection
