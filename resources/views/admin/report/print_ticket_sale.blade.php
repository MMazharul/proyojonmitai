<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 3:23 PM
 */

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    <title>  {{ $page_title }}  </title>
    <style>
        * { margin: 0; padding: 0; }
        body {
            font: 14px/1.4 Helvetica, Arial, sans-serif;
        }
        #page-wrap { width: 720px; margin: 0 auto; }

        table {
            display: table;
            border-collapse: collapse;
            border-spacing: 0;
            color: #0a0a0a !important;
            width: 100% !important;
        }

        .table tbody tr td, .table thead tr th, .table tbody tr th, .table thead tr td {
            padding: 3px; !important;
            border: 0.3px solid rgba(1, 1, 1, 0.74) !important;
        }
        .table-border-padding{
            border: 0.3px solid rgba(1, 1, 1, 0.74) !important;
            padding: 3px; !important;
        }
        .text-center{
            text-align: center !important;
        }

        .text-right{
            text-align: right !important;
        }

        #logo { text-align: right; width: 70px; height: 50px; overflow: hidden; }


        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
</head>
<body>
<div id="page-wrap">


    <div style="width: 100%;">
        <table style="margin-bottom: 10px; width: 100%;  margin-top: 5mm;">
            <tr>
{{--                @if($company_logo)--}}
{{--                    <th style="text-align: right; border: 0 !important;" width="30%">--}}
{{--                        <img height="100px" width="100px" src="{{ $company_logo }}" alt="{{ $company_name }}" id="logo"/>--}}
{{--                    </th>--}}
{{--                @endif--}}
                <td style="text-align: center; border: 0 !important;" width="100%" >
                    <h2 style="margin-bottom: 5px;"> Shadhin travels ltd </h2>
{{--                    <p> Address: {{ $company_address }}</p>--}}
{{--                    <p> {{ $company_city.', '.$company_country }}</p>--}}
{{--                    <p> Phone: {{ $company_phone }}</p>--}}
                </td>
            </tr>
            <tr>
                <th colspan="2" style="border: 0 !important; padding-top: 5px; text-align: center;">
                    <h3>{{ $page_title }}</h3>
                </th>
            </tr>
        </table>
    </div>


    <div style="overflow: hidden; clear: both;">
        <table cellspacing="0" width="100%" class="table">
            <thead>
            <tr>
                <th>SL</th>
                <th>Route Name</th>
                <th>Agent Name</th>
                <th class="text-right">Balance</th>

            </tr>
            </thead>
            <tbody>
            @php
                $total_fare = 0;
            @endphp

            @foreach($ticket_sales as $k=>$data)
                <tr>
                    <td  class="text-center">{{ $k+1 }}</td>
                    <td>{{ $data->tripRoute->name }}</td>
                    <td>{{ $data->agent->full_name }}</td>
                    <td class="text-right">{{ $data->total_fare }}</td>

                </tr>
                @php
                    $total_fare += $data->total_fare;

                @endphp
                @if($loop->last)
                    <tr>
                        <th colspan="3"> Total </th>
                        <th class="text-right">{{ create_money_format($total_fare) }}</th>
                    </tr>
                @endif
            @endforeach

            </tbody>
        </table>
    </div>
</div>
</body>
</html>
