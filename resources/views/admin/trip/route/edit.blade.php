@extends('admin.layout.master')
@section('import-css')
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-tagsinput.css')}}">
@endsection

@section('css')
    <style>
        .btn-light {
            background-color: #f8f9fa;
            border-color: #ced4da;
        }

        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #fffdfd;
            background: #555f69;
            border-radius: 2px;
        }
        .bootstrap-tagsinput{
            width: 100%;
            min-height: 40px;
        }
        .bootstrap-select>.dropdown-toggle.bs-placeholder, .bootstrap-select>.dropdown-toggle.bs-placeholder:active, .bootstrap-select>.dropdown-toggle.bs-placeholder:focus, .bootstrap-select>.dropdown-toggle.bs-placeholder:hover {
            color: #1d1919;
        }
    </style>
@stop


@section('body')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('manage-route')}}" class="btn btn-success btn-md float-right">
                <i class="fa-fw fas fa-map"></i> All Route
            </a>
        </div>

        <form role="form" method="POST" action="{{route('manage-route.update',$route)}}" name="editForm"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{method_field('put')}}

            <div class="card-body">
                <div class="form-row">
                    <div class="offset-md-1 col-md-9 mb-3">
                        @include('errors.error')
                    </div>
                </div>


                <div class="form-row">
                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Route Name <span class="error">*</span></strong></label>
                        <input type="text" name="name" placeholder="Route Name" value="{{$route->name}}"
                               class="form-control form-control-lg @if ($errors->has('name'))  is-invalid @endif">
                    </div>

                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Start Point <span class="error">*</span></strong></label>
                        <select name="start_point"
                                class="form-control form-control-lg selectpicker @if ($errors->has('start_point'))  is-invalid @endif"
                                data-live-search="true">
                            <option value="">Select Start Point</option>
                            @foreach($tripLocation as $data)
                                <option value="{{$data->id}}" @if($data->id == $route->start_point) selected @endif>{{$data->name}}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>End Point <span class="error">*</span></strong></label>
                        <select name="end_point"
                                class="form-control form-control-lg selectpicker @if ($errors->has('end_point'))  is-invalid @endif"
                                data-live-search="true">
                            <option value="">Select End Point</option>
                            @foreach($tripLocation as $data)
                                <option value="{{$data->id}}"  @if($data->id == $route->end_point) selected @endif>{{$data->name}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="offset-md-1 col-md-9 mb-3">
                        <label class="mb-1"><strong>Stoppage Points <span class="error">*</span></strong></label>
                        <input type="text"  name="stoppage" placeholder="Stoppage Points" value="{{$route->stoppage}}" data-role="tagsinput"
                               class="stoppage form-control form-control-lg @if ($errors->has('stoppage'))  is-invalid @endif">
                    </div>

                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Distance <span class="error">*</span></strong></label>
                        <div class="input-group mb-3">
                            <input type="text" name="distance" placeholder="Distance" value="{{$route->distance}}"
                                   class="form-control form-control-lg @if ($errors->has('distance'))  is-invalid @endif">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>kilometer</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Approximate Time <span class="error">*</span></strong></label>
                        <div class="input-group mb-3">
                            <input type="text" name="approximate_time" placeholder="Approximate Time"
                                   value="{{$route->approximate_time}}"
                                   class="form-control form-control-lg @if ($errors->has('approximate_time'))  is-invalid @endif">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>hours</strong></span>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="form-row">
                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Status</strong></label>
                        <input type="checkbox" name="status" @if($route->status == 1) checked @endif data-toggle="toggle" data-on="Active" data-off="DeActive"
                               checked
                               data-onstyle="success" data-offstyle="danger" data-width="100%">
                    </div>
                </div>

            </div>

            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save</button>
            </div>

        </form>
    </div>


@endsection

@section('import-script')
    <script src="{{asset('assets/admin/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-tagsinput.js')}}"></script>

@stop
@section('script')
    <script>
        // $("input").tagsinput('items')
    </script>

@stop