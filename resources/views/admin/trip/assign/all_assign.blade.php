@extends('admin.layout.master')
@section('body')
    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <div class="float-left">
                <h2 class="mb-4">{{$page_title}}</h2>
            </div>


        </div>
        <div class="card-body">
            @include('errors.error')

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Trip Id</th>
                    <th>Registration No.</th>
                    <th>Route Name</th>
                    <th>Trip Start Date</th>
                    <th>Trip End Date</th>

                    <th>Status</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>

                @foreach($tripAssign as $k=>$data)
                    <tr>
                        <td data-label="SL">{{++$k}}</td>
                        <td data-label="Trip Id"><strong>{{$data->id_no}}</strong></td>
                        <td data-label="Registration No.">
                            <strong>{{$data->coach_no}}</strong>
                        </td>
                        <td data-label="Route Name">
                            <strong>{{$data->tripRoute->name}}</strong>
                        </td>
                        <td data-label="Trip Start Date">{{date('d M Y h:i A',strtotime($data->start_date))}}</td>
                        <td data-label="Trip End Date">{{date('d M Y h:i A',strtotime($data->end_date))}}</td>
                        <td data-label="Status">
                            <span class="badge  badge-pill  badge-{{ $data->status ==0 ? 'danger' : 'success' }}">{{ $data->status == 0 ? 'Deactive' : 'Active' }}</span>
                        </td>
                        <td data-label="Action">
                            <a href="{{route('trip-assign.edit',$data->id)}}"
                             class="btn btn-success  btn-icon btn-pill" title="Edit">
                              <i class="fa fa-pencil-alt"></i>
                            </a>

                             <a href="{{route('admin_daily_report',['id'=>$data->id_no,'type'=>'view'])}}"
                             class="btn btn-info  btn-icon btn-pill" title="Chalan Sheet">
                              <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{route('trip-assign.delete',['id'=>$data->id])}}"
                           class="btn btn-danger  btn-icon btn-pill" title="Delete">
                            <i class="fa fa-trash"></i>
                          </a>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            {!! $tripAssign->render() !!}
        </div>
    </div>

@endsection

@section('script')
@endsection
