@extends('admin.layout.master')
@section('import-css')
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery.simple-dtpicker.css')}}">
@endsection

@section('css')
    <style>
        .btn-light {
            background-color: #f8f9fa;
            border-color: #ced4da;
        }
        .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:active, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
            color: #1d1919;
        }
    </style>
@stop


@section('body')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            <a href="{{route('trip-assign')}}" class="btn btn-success btn-md float-right">
                <i class="fa-fw fas fa-map"></i> All Trip/Assign
            </a>
        </div>

        <form role="form" method="POST" action="{{route('trip-assign.update',$tripAssign)}}" name="editForm"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{method_field('put')}}

            <div class="card-body">
                <div class="form-row">
                    <div class="offset-md-1 col-md-9 mb-3">
                        @include('errors.error')
                    </div>
                </div>


                <div class="form-row">
                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Fleet Registration No. <span class="error">*</span></strong></label>
                        <select name="fleet_registration_id"
                                class="form-control form-control-lg selectpicker @if ($errors->has('fleet_registration_id'))  is-invalid @endif"
                                data-live-search="true">
                            <option value="">Select a Fleet Registration</option>
                            @foreach($fleet_registration as $data)
                                <option value="{{$data->id}}"  @if($tripAssign->fleet_registration_id == $data->id) selected @endif>{{$data->reg_no}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Route Name <span class="error">*</span></strong></label>
                        <select name="trip_route_id" id="trip_route_id"
                                class="form-control form-control-lg selectpicker @if ($errors->has('trip_route_id'))  is-invalid @endif"
                                data-live-search="true">
                            <option value="">Select a route</option>
                            @foreach($tripRoute as $data)
                                <option value="{{$data->id}}" @if($tripAssign->trip_route_id == $data->id) selected  @endif>{{$data->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Start Point <span class="error">*</span></strong></label>
                        <select name="start_point" id="start_point"
                                class="form-control form-control-lg  @if ($errors->has('start_point'))  is-invalid @endif">
                            @if(isset($tripAssign->start_point))
                            <option value="{{$tripAssign->start_point}}">{{$tripAssign->start_point}}</option>
                            @endif
                        </select>
                    </div>

                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>End Point <span class="error">*</span></strong></label>
                        <select name="end_point" id="end_point"
                                class="form-control form-control-lg  @if ($errors->has('end_point'))  is-invalid @endif">
                            @if(isset($tripAssign->end_point))
                                <option value="{{$tripAssign->end_point}}">{{$tripAssign->end_point}}</option>
                            @endif

                        </select>
                    </div>


                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Trip Start Date <span class="error">*</span></strong></label>
                        <input name="start_date" class="form-control form-control-lg datetimepicker" type="text"
                               placeholder="Trip End Date" id="start_date" value="{{$tripAssign->end_date}}">
                    </div>

                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Trip End Date <span class="error">*</span></strong></label>
                        <input name="end_date" class="form-control form-control-lg datetimepicker" type="text"
                               placeholder="Trip End Date" id="end_date" value="{{$tripAssign->end_date}}">
                    </div>

                </div>


                <div class="form-row">
                    <div class="offset-md-1 col-md-9 mb-3">
                        <label><strong>Status</strong></label>
                        <input type="checkbox" name="status" data-toggle="toggle" data-on="Active" data-off="DeActive"
                              @if($tripAssign->status == 1) checked @endif data-onstyle="success" data-offstyle="danger" data-width="100%">
                    </div>
                </div>

            </div>

            <div class="card-footer bg-white">
                <button class="btn btn-success btn-block btn-lg" type="submit">Save</button>
            </div>

        </form>
    </div>



@endsection

@section('import-script')

    <script src="{{asset('assets/admin/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery.simple-dtpicker.js')}}"></script>

@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datetimepicker').appendDtpicker();
        })

        $(document).on('change', "#trip_route_id", function (e) {

            e.preventDefault();

            var trip_route_id = $("#trip_route_id").val();
            let start_point = $('#start_point');
            let end_point = $('#end_point');

            start_point.empty();
            end_point.empty();



            if(trip_route_id == ''){
                toastr.error("Please Enter Route Name");
            }
            else{
                $.ajax({
                    type: "get",
                    url: "{{route('findcounter')}}",
                    data: {
                        trip_route_id: trip_route_id,
                    },
                    success: function (data) {

                        start_point.append('<option value="0" disabled="true" selected="true">Select Start Point</option>');
                        end_point.append('<option value="0" disabled="true" selected="true">Select End Point</option>');

                        for(let startform  of data.from_counter)
                        {
                            console.log(startform.counter_name);
                            start_point.append('<option value="'+ startform.counter_name +'">'+startform.counter_name+'</option>');
                        }

                        for(let endform of data.to_counter)
                        {
                            console.log(endform.counter_name);
                            end_point.append('<option value="'+ endform.counter_name +'">'+endform.counter_name+'</option>');
                        }

                    },

                    error: function (res) {
                        //console.log(res);
                    }
                });

            }
        });

    </script>
@stop