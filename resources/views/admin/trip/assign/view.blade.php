<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 12:51 PM
 */
?>

@extends('admin.layout.master')

@section('body')

    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">

        <div class="card-body">
          <div>

              <a class="btn btn-sm  btn-primary pull-right" href="{{route('all-trip-assign')}}"> <i class="fa fa-backward"></i> Back </a>
              <a class="btn btn-sm  btn-primary pull-right" target="_blank" href="{{route('trip-assign-view.print',['id'=>$id])}}" id="btn-print"> <i class="fa fa-print"></i> Print </a>

          </div>

        </div>


        <div class="card-body">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Passenger Name</th>
                    <th>Agent Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Seat No</th>
                    <th>Balance</th>
                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>
                @php
                    $total_fare = 0;

                @endphp


                    @foreach($ticket_sales as $k=>$data)


                            <tr>
                                <td>{{ $k+1 }}</td>
                                <td>{{ $data->passenger_name}}</td>
                                <td>{{ $data->username}}</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->phone }}</td>
                                <td>{{ $data->seat_number }}</td>
                                <td>{{ $data->total_fare }}</td>

                            </tr>

                            @php
                                $total_fare += $data->total_fare;

                            @endphp
                            @if($loop->last)
                                <tr>
                                    <th colspan="6"> Total </th>
                                    <th >{{ create_money_format($total_fare) }}</th>
                                </tr>
                            @endif
                    @endforeach



                </tbody>
            </table>


            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Driver Name</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>@if(isset($driverInfo)){{$driverInfo->driver_name}} {{$driverInfo->driver_phone}}@endif</th>
                </tr>

                <tr>
                  <th>Superviser Name</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th >@if(isset($driverInfo)){{$driverInfo->superviser_name}} {{($driverInfo->superviser_phone)}}@endif</th>
                </tr>

              </thead>

            </table>
        </div>
    </div>


@endsection
@section('script')


    <script src="{{ asset('public/assets/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $( ".datepicker" ).datepicker();

            $('#btn-print, .btn-print').printPage();
        });
    </script>

@endsection
