@extends('admin.layout.master')

@section('body')
		<div class="card">

			<div class="card-header bg-white font-weight-bold">
				<h4>SMS Template</h4>
			</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
							<tr>
								<th> # </th>
								<th> CODE </th>
								<th> DESCRIPTION </th>
							</tr>
							</thead>
							<tbody>


							<tr>
								<td> 1 </td>
								<td> <pre>&#123;&#123;message&#125;&#125;</pre> </td>
								<td> Details Text From Script</td>
							</tr>

							<tr>
								<td> 2 </td>
								<td> <pre>&#123;&#123;number&#125;&#125;</pre> </td>
								<td> Users Number. Will Pull From Database</td>
							</tr>



							</tbody>
						</table>
					</div>
				</div>
			</div>





		<div class="card mt-4">
			<div class="card-header bg-white font-weight-bold">
				<h4>SMS API</h4>
			</div>
			<div class="card-body">
				<form role="form" method="POST" action="{{route('sms.update')}}" >
					{{ csrf_field() }}
					<div class="form-body">
						<div class="form-group">
							<input type="text" name="smsapi" id="smsapi" class="form-control" value="{{$temp->smsapi}}">
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-block btn-lg">Update</button>

				</form>
			</div>
		</div>


	
	@endsection

@section('js')
@stop