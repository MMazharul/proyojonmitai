@extends('agent.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
@stop

@section('body')
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">
        <div class="card-header bg-white font-weight-bold">
            <form action="{{route('agent.trip_find')}}" method="get">
                <div class="form-row">
                  <div class="col-lg-2">
                      <input type="text" name="from_date" id="datetimepicker2"  value="{{old('date')}}" class="form-control form-control-lg" placeholder="From Date">
                  </div>
                  <div class="col-lg-2">
                      <input type="text" name="to_date" id="datetimepicker3"  value="{{old('date')}}" class="form-control form-control-lg" placeholder="To Date">
                  </div>
                    <div class="col-lg-2">
                        <input type="text" name="from_location" value="{{old('from_location')}}" class="form-control form-control-lg" id="fromAutoComplete" placeholder="From Location">
                    </div>
                    <div class="col-lg-2">
                        <input type="text" name="to_location"  value="{{old('to_location')}}" id="toAutoComplete" class="form-control form-control-lg" placeholder="To Location">
                    </div>
                    <div class="col-lg-2">

                      <select class="form-control form-control-lg" name="coach_no">
                          <option disabled selected>Select Coach</option>
                          @foreach($all_coach as $key=>$coach)
                              <option value="{{$key}}">{{$key}}</option>
                          @endforeach

                      </select>

                    </div>
                        <div class="col-lg-2">
                            <input type="text" name="trip_id"  value="{{old('trip_id')}}" id="toAutoComplete" class="form-control form-control-lg" placeholder="Trip ID">
                        </div>
                </div><br>

                <div class="form-row">
                    <div class="col-lg-2">
                    <button type="submit" class="btn btn-success btn-lg  h-serch mamunur_rashid_form_sm">
                        <i class="fa fa-search"></i> SEARCH
                    </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Operator</th>
                    <th>Departure</th>
                    <th>Arrival</th>
                    <th>Total Seat</th>
                    <th>Fare</th>
                    <th>Calan Sheet</th>
                </tr>
                </thead>

                <tbody>
                @if(count($trip_asign)>0)
                    @foreach($trip_asign as $data)
                        <tr>
                            <td>
                                <div class="t-box-1">
                                    <h5>{{$data->tripRoute->name}}</h5>
                                    <strong>{{date('d M Y',strtotime($data->start_date))}}</strong>
                                </div>
                            </td>
                            <td>
                                <h5>{{date('h:i A',strtotime($data->start_date))}}</h5>
                                <strong class="text-success">{{$data->tripRoute->start_point_name}}</strong>
                            </td>

                            <td>
                                <strong class="text-success">{{$data->tripRoute->end_point_name}}</strong>
                            </td>

                            <td>
                                <div class="p-img">
                                    <strong>{{$data->fleetRegistration->total_seat}} seats</strong>
                                </div>
                                <b class="text-success">{{$data->fleetRegistration->fleetType->name}}</b>
                            </td>
                            @php
                                $ticketPrice =  \App\TicketPrice::where('trip_route_id',$data->trip_route_id)->where('fleet_type_id',$data->fleetRegistration->fleet_type_id)->latest()->first();
                            @endphp


                            <td>

                                <div class="p-img">
                                    @if($ticketPrice)
                                        <strong>{{($ticketPrice->price) ?? '' }} {{$basic->currency}}</strong>

                                    @else
                                        <strong class="text-danger">-</strong>
                                    @endif
                                </div>
                            </td>

                            <td>
                                <div class="l-box">
                                    <div class="media">
                                        <div class="media-body align-self-end">
                                            <div class="link">
                                                <a href="{{route('agent_daily_report',['id'=>$data->id_no,'type'=>'view'])}}" title="Calan Sheet" class="btn btn-icon btn-pill btn-primary">
                                                    <i class="fa fa-eye"></i> </a>
                                                    <a href="{{route('agent_daily_report',['id'=>$data->id_no,'type'=>'print'])}}" title="Calan Sheet" class="btn btn-icon btn-pill btn-primary">
                                                        <i class="fa fa-print"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">
                            <h4 class="text-center text-danger margin-top-40 margin-bottom-60">No result found!!</h4>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>



@endsection

@section('script')
    <script src="{{asset('assets/front/js/jquery.autocomplete.js')}}"></script>
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>
    {{--<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
    <script type="text/javascript">

        $(document).ready(function () {
            var states = {!! $tripFrom !!};

            $(function () {
                $("#fromAutoComplete").autocomplete({
                    source: [states]
                });
            });
            $(function () {
                $("#toAutoComplete").autocomplete({
                    source: [states]
                });
            });


            $("#datetimepicker2").flatpickr({
                minDate: "",
                maxDate: new Date().fp_incr(50), // 14 days from now
                dateFormat: "d M Y",
            });
            $("#datetimepicker3").flatpickr({
                minDate: "",
                maxDate: new Date().fp_incr(50), // 14 days from now
                dateFormat: "d M Y",
            });


        });

    </script>
@stop
