@extends('agent.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
@stop

@section('body')
    <h2 class="mb-4">{{$page_title}}</h2>

    <div class="card">



        <div class="card-header bg-white font-weight-bold">
            <form action="{{route('agent_report')}}" method="get">

              <div>
                  <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>
              </div><br>

                <div class="form-row">
                    <div class="col-lg-3">
                        <input type="text" name="from_date" id="datetimepicker2"  value="{{old('from_date')}}" class="form-control " placeholder="From Date">
                    </div>
                    <div class="col-lg-3">
                        <input type="text" name="to_date" id="datetimepicker3"  value="{{old('to_date')}}" class="form-control" placeholder="To Date">
                    </div>
                    <div class="col-lg-3">
                        <select class="select2 form-control" name="trip_id">
                            <option disabled selected>Select Coach</option>
                            @foreach($all_coach as $key=>$coach)
                                <option value="{{$coach}}">{{$key}}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-success h-serch mamunur_rashid_form_sm">
                            <i class="fa fa-search"></i> SEARCH
                        </button>
                    </div>

                </div>

            </form>
        </div>

        <div class="card-body">

            <table class="table table-bordered">
                <thead>
                <tr>
                <th>Trip ID</th>
                <th>Total Seat Booking</th>
                <th>available Seat</th>
                <th>Total Discount</th>
                <th>Total Price</th>
                <th>Cross Total</th>

                </tr>
                </thead>
                <tbody>
                @if(count($ticket_sales)>0)
                    @php
                        $cross_total = 0;
                        $total_discount = 0;
                    @endphp

                    @foreach($ticket_sales as $data)
                        <tr>
                            <td>
                                <div class="t-box-1">
                                    <h5>{{$data->id_no}}</h5>
                                </div>
                            </td>
                            <td>
                                <h5>{{$data->total_seat}}</h5>

                            </td>

                            <td>
                                <strong class="text-success">{{36-$data->total_seat}}</strong>
                            </td>

                            <td>
                                <div class="p-img">
                                    <strong>{{$data->total_discount}} </strong>
                                </div>

                            </td>

                            <td>
                                <div class="p-img">
                                    <strong>{{$data->total_fare}} </strong>
                                </div>

                            </td>

                            <td>
                                <div class="p-img">
                                    <strong>{{$data->cross_total}} </strong>
                                </div>

                            </td>
                        </tr>
                        @php
                            $cross_total += $data->cross_total;
                            $total_discount += $data->total_discount;
                        @endphp
                        @if($loop->last)
                            <tr>
                                <th colspan="5">Cross Total</th>
                                <td>{{ create_money_format($cross_total) }}</td>
                            </tr>
                            <tr>
                                <th colspan="5">Discount Total</th>
                                <td>{{ create_money_format($total_discount) }}</td>
                            </tr>
                            <tr>
                                <th colspan="5">Grand Total</th>
                                <td>{{ create_money_format($total_discount+$cross_total) }}</td>
                            </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">
                            <h4 class="text-center text-danger margin-top-40 margin-bottom-60">No result found!!</h4>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/front/js/jquery.autocomplete.js')}}"></script>
    <script src="{{asset('assets/front/js/flatpickr.js')}}"></script>
    {{--<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>--}}
    <script type="text/javascript">

        $(document).ready(function () {


            $("#datetimepicker2").flatpickr({
                minDate: "",
                maxDate: new Date().fp_incr(50), // 14 days from now
                dateFormat: "d M Y",
            });
            $("#datetimepicker3").flatpickr({
                minDate: "",
                maxDate: new Date().fp_incr(50), // 14 days from now
                dateFormat: "d M Y",
            });


        });

    </script>
@stop
