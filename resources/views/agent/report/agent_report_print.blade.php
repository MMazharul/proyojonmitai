<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 18-Dec-19
 * Time: 3:23 PM
 */

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">


    <title>  {{ $page_title }}  </title>
    <style>
        * { margin: 0; padding: 0; }
        body {
            font: 14px/1.4 Helvetica, Arial, sans-serif;
        }
        #page-wrap { width: 820px; margin: 0 auto; }

        table {
            display: table;
            border-collapse: collapse;
            border-spacing: 0;
            color: #0a0a0a !important;
            width: 100% !important;
        }

        .table tbody tr td, .table thead tr th, .table tbody tr th, .table thead tr td {
            padding: 3px; !important;
            border: 0.3px solid rgba(1, 1, 1, 0.74) !important;
        }
        .table-border-padding{
            border: 0.3px solid rgba(1, 1, 1, 0.74) !important;
            padding: 3px; !important;
        }
        .text-center{
            text-align: center !important;
        }

        .text-right{
            text-align: right !important;
        }
        .row{
            width:100%px;
        }
        .col1{
            float:left;
            width:70%;
        }
        th td{
            font-size:10px;
        }

    .tab1 th{
        width:70px;
        font-size: 12px;
        text-align: center;

    }
.tab1 td{
     width:70px;
        font-size: 12px;
        text-align: center;
}


        #logo { text-align: right; width: 70px; height: 50px; overflow: hidden; }


        @media print
        {
            .btn-print
            {
                display: none !important;
            }
        }
    </style>
</head>
<body>
    <button style="padding:10px;widht:80px;margin-top:5px;margin-left:5px" class="btn btn-sm btn-success btn-lg btn-print" onClick="window.print();"> <i class="fa fa-print"></i> Print</button>

<div id="page-wrap">

    <div style="overflow: hidden; clear: both;">

      <div class="card-body">
        <div class="card-header">
          <h3 style="text-align:center">{{ $page_title }}</h3><br>
        </div>

          <table class="table table-bordered">
              <thead>
                  <th>Trid ID</th>
                  <th>Total Seat Booking</th>
                  <th>available Seat</th>
                  <th>Total Price</th>

              </tr>
              </thead>
              <tbody>
              @if(count($ticket_sales)>0)
              @php
                  $total_fare = 0;
              @endphp

                  @foreach($ticket_sales as $data)
                      <tr>
                          <td>
                              <div class="t-box-1">
                                  <h5>{{$data->id_no}}</h5>
                              </div>
                          </td>
                          <td>
                              <h5>{{$data->total_seat}}</h5>

                          </td>

                          <td>
                              <strong class="text-success">{{36-$data->total_seat}}</strong>
                          </td>

                          <td>
                              <div class="p-img">
                                  <strong>{{$data->total_fare}} </strong>
                              </div>

                          </td>
                      </tr>
                      @php
                          $total_fare += $data->total_fare;
                      @endphp
                      @if($loop->last)
                          <tr>
                              <th colspan="3"> Total Amount </th>
                              <td>{{ create_money_format($total_fare) }}</td>
                          </tr>
                      @endif
                  @endforeach
              @else
                  <tr>
                      <td colspan="7">
                          <h4 class="text-center text-danger margin-top-40 margin-bottom-60">No result found!!</h4>
                      </td>
                  </tr>
              @endif
              </tbody>
          </table>
      </div>

    </div>

      <div class="ticket-footer" style="text-align: center;margin-top: 0px;"><br>
        <p style="font-size:12px;font-weight:normal;"><b>Software Developed and Maintaince By : Rcreation || 01722964303,01813316786</b> </p><br>

    </div>


</div>
</body>
</html>
