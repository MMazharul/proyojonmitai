@extends('agent.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/jquery.autocomplete.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/flatpickr.min.css')}}">
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">--}}
@stop

@section('body')


    <div>
{{--        <a class="btn btn-sm  btn-primary pull-right" href="{{ route(Route::current()->getName()) == $full_url ? route(Route::current()->getName())."?" : $full_url.'&' }}type=print" id="btn-print"> <i class="fa fa-print"></i> Print </a>--}}
        <a class="btn btn-lg  btn-primary pull-right" href="{{route('agent_daily_report',['id'=>$trip_asign->id_no,'type'=>'print'])}}" id="btn-print"> <i class="fa fa-print"></i> Print </a>
    </div>
    <div class="card">
        <div class="card-header">
            <div style="width: 100%;">
                <div class="row">
                     <div class="col-md-12" >
                        <h2 style="text-align:center;margin-bottom: 5px;"> {{$trip_asign->fleetRegistration->company}}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">

            <table class="table table-bordered">
            <tbody>
            <tr>
              <th>Trip No</th><td>{{$trip_asign->id_no}}</td>
              <th>Driver</th><td>{{$driveInfo['driver_name']}}</td>
              <th>Guider</th><td>{{$driveInfo['superviser_name']}}</td>
              <th>Helper</th><td>Karim</td>
            </tr>
            </tbody>
            </table>

            <table class="table table-bordered">
                <tr><th>Coach No</th><td>{{$trip_asign->coach_no}}</td>
                  <th>Starting Place</th><td>{{$trip_asign->start_point}}</td>
                  <th>Journey by date</th><td>{{date('d M Y h:i A',strtotime($trip_asign->start_date))}}</td></tr>
                <tr><th>Route Name</th><td>{{$trip_asign->tripRoute->name}}</td><th>Total Sold</th><td>{{$sold_ticket}}</td><th>Total Available</th><td>{{$trip_asign->fleetRegistration->total_seat - $sold_ticket}}</td></tr>
            </table>


            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Seats</th>
                    <th>Pass. Name</th>
                    <th>Mobile</th>
                    <th>Ticket No</th>
                    <th>Issue By</th>
                    <th>Issue Counter</th>
                    <th>Discount</th>
                    <th>Total</th>
                    <th>Cross Total</th>

                    <!-- <th>Commission</th> -->
                </tr>
                </thead>
                <tbody>
                  @php
                      $total_fare = 0;
                      $discount=0;
                      $sub_total=0;
                  @endphp

                @foreach($ticket_sale as $k=>$data)
                  <tr>
                    <td>{{++$k}}</td>
                    <td>{{$data->seat_number}}</td>
                    <td>{{$data->passenger_name	}}</td>
                    <td>{{$data->phone}}</td>
                    <td>{{$data->pnr}}</td>

                    <td>{{$data->agent->username}}</td>
                    <td>@if(isset($data->counter->counter_name)){{$data->counter->counter_name}}@endif</td>
                    <td>{{$data->discount}}</td>
                    <td>{{$data->total_fare}}</td>
                    <td>{{$data->cross_total}}</td>

                  </tr>
                  @php
                      $sub_total += $data->cross_total;
                      $discount += $data->discount;
                  @endphp
                  @if($loop->last)

                      <tr>
                          <th colspan="9">Cross Total </th>
                          <td>{{ create_money_format($sub_total) }}</td>

                      </tr>
                      <tr>
                          <th colspan="9"> Discount Total </th>
                          <td>{{ create_money_format($discount) }}</td>
                      </tr>

                      <tr>
                          <th colspan="9">Grand Total</th>
                          <td>{{ create_money_format($discount+$sub_total) }}</td>
                      </tr>

                  @endif
                @endforeach


                </tbody>
            </table>



        </div>


    <!-- <div style="width: 100%;">

    </div> -->
    </div>
    <div class="card">
        <div class="card-header">
          <h4 class="text-center">Counter Ticket Sell</h4>
        </div>
        <div class="card-body">
          <table class="table table-bordered">
              <thead>
              <tr>
                  <th>Sl No</th>
                  <th>Counter Name</th>
                  <th>Total Seat Sold</th>
                  <th>Total Price</th>

                  <!-- <th>Commission</th> -->
              </tr>
              </thead>
              <tbody>
                @php
                    $total_fare = 0;
                @endphp

              @foreach($counter_sale as $k=>$data)
                <tr>
                  <td>{{++$k}}</td>
                  <td>@if(isset($data->counter->counter_name)){{$data->counter->counter_name}}@endif</td>
                  <td>{{$data->total_seat	}}</td>
                  <td>{{$data->total_fare}}</td>
                </tr>
                @php
                    $total_fare += $data->total_fare;
                @endphp
                @if($loop->last)
                    <tr>
                        <th colspan="3"> Total </th>
                        <td>{{ create_money_format($total_fare) }}</td>
                    </tr>
                @endif
              @endforeach


              </tbody>
          </table>
        </div>
    </div>

@endsection

@section('script')


    <script src="{{ asset('public/assets/jquery.printPage.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
            $( ".datepicker" ).datepicker();

            $('#btn-print, .btn-print').printPage();
        });
    </script>

@endsection
