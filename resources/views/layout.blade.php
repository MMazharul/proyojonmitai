<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="zxx">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title> @isset($page_title)  {{__($page_title)}} | @endisset  {{__($basic->sitename)}}  </title>
    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/logo/favicon.png')}}" type="image/x-icon">

    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,600i,700" rel="stylesheet">
    <!--Bootstrap Stylesheet-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/bootstrap.min.css')}}">

@yield('import-css')
<!--Owl Carousel Stylesheet-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/plugins/owl.carousel.min.css')}}">
    <!--Slick Slider Stylesheet-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/plugins/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/plugins/slick.css')}}">
    <!--Font Awesome Stylesheet-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/font-awesome.min.css')}}">
    <!--Animate Stylesheet-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/plugins/animate.css')}}">
    <link href="{{asset('assets/admin/css/toastr.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/admin/css/sweetalert.css')}}" rel="stylesheet">
    <!--Main Stylesheet-->
    <link rel="stylesheet"  href="{{asset('assets/front/css/style.php')}}?color={{ $basic->color }}">
    <!--Responsive Stylesheet-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/homePageResponsive.css')}}">
    @yield('style')

</head>

<body class="body-class @yield('force-css')">
<!--Start Preloader-->
<div class="site-preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!--End Preloader-->

{!! $basic->fb_comment !!}

<!--Start Body Wrap-->
<div id="body-wrap">
    <div id="main-menu">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{asset('assets/images/logo/logo.png')}}" alt="..">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link  @if(request()->is('/')) active @endif" href="{{url('/')}}">@lang('Home')</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('about-us')) active @endif" href="{{route('about')}}">@lang('About') </a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('blog')) active @endif" href="{{route('blog')}}">@lang('Blog')</a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('faqs')) active @endif" href="{{route('faqs')}}">@lang('Faq')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(request()->is('contact-us')) active @endif" href="{{route('contact')}}">@lang('Contact')</a>
                        </li>


                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle
                             @if(request()->is('user/ticket-cancel')) active
                             @elseif(request()->is('ticket-print')) active
                             @endif
                            " href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">@lang('Booking')
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <a class="dropdown-item" href="{{route('ticket-cancel')}}">@lang('Ticket Cancel')</a>
                                <a class="dropdown-item" href="{{route('ticket-print')}}">@lang('Print Download')</a>
                            </div>
                        </li>

                        @include('partials.lang')
                        @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle mamunur_rashid_top_book_btn" href="#"
                               id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                {{Auth::user()->username}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <a class="dropdown-item" href="{{route('home')}}">@lang('My Trips')</a>
                                <a class="dropdown-item" href="{{route('withdraw.money')}}">@lang('Withdraw') (<strong> {{Auth::user()->balance}} {{__($basic->currency)}} )</strong></a>
                                <a class="dropdown-item" href="{{route('user.withdrawLog')}}">@lang('Withdraw List')</a>
                                <a class="dropdown-item" href="{{route('user.trx')}}">@lang('Transaction Log')</a>

                                <a class="dropdown-item" href="{{route('edit-profile')}}">@lang('My Profile')</a>
                                <a class="dropdown-item" href="{{route('user.change-password')}}">@lang('Change Password')</a>
                                <a href="{{ route('logout') }}" class="dropdown-item"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Sign Out')</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">{{ csrf_field() }}</form>
                            </div>
                        </li>
                        @endauth
                    </ul>


             <a class="mamunur_rashid_top_book_btn" href="{{route('search')}}">@lang('Buy Ticket')</a>

                    @guest

                        <a class="mamunur_rashid_top_book_btn" href="{{route('login')}}">@lang('Sign In')</a>
                    @endguest
                </div>
            </div>
        </nav>
    </div>

    @yield('content')


    @include('partials.footer')
</div>
<!--End Body Wrap-->

<!--jQuery JS-->
<script src="{{asset('assets/front/js/jquery.2.1.2.min.js')}}"></script>
<!--Bootstrap JS-->
<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
<!--Counter JS-->
<script src="{{asset('assets/front/js/plugins/waypoints.js')}}"></script>
<script src="{{asset('assets/front/js/plugins/jquery.counterup.min.js')}}"></script>

<script src="{{asset('assets/admin/js/toastr.min.js')}}"></script>
<script src="{{asset('assets/admin/js/sweetalert.js')}}"></script>


@yield('script')
<!--Owl Carousel JS-->
<script src="{{asset('assets/front/js/plugins/owl.carousel.min.js')}}"></script>
<!--Venobox JS-->
<script src="{{asset('assets/front/js/plugins/venobox.min.js')}}"></script>
<!--Slick Slider JS-->
<script src="{{asset('assets/front/js/plugins/slick.min.js')}}"></script>
<!--Main-->
<script src="{{asset('assets/front/js/custom.js')}}"></script>

@yield('js')
@if (session('success'))
    <script>
        $(document).ready(function () {
            swal("Success!", "{{ session('success') }}", "success");
        });
    </script>
@endif

@if (session('alert'))
    <script>
        $(document).ready(function () {
            swal("Sorry!", "{{ session('alert') }}", "error");
        });
    </script>
@endif

@if(Session::has('message'))
    <script>
        var type = "{{Session::get('alert-type','info')}}";
        switch (type) {
            case 'info':
                toastr.info("{{Session::get('message')}}");
                break;
            case 'warning':
                toastr.warning("{{Session::get('message')}}");
                break;
            case 'success':
                toastr.success("{{Session::get('message')}}");
                break;
            case 'error':
                toastr.error("{{Session::get('message')}}");
                break;
        }
    </script>
@endif

</body>

</html>
