<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ride_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driver_name')->nullable();
            $table->integer('bus_id')->nullable();
            $table->string('driver_phone')->nullable();
            $table->string('superviser_name')->nullable();
            $table->string('superviser_phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ride_infos');
    }
}
