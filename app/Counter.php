<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Counter extends Model
{

  protected $guarded = [];

  public function tripRoute()
  {
      return $this->belongsTo('App\TripRoute','trip_route_id','id');
  }

  public function location()
  {
    return $this->belongsTo('App\TripLocation','location_id','id');
  }

  public function  agents(){
      return $this->hasMany('App\Agent','counter_id');
  }

  public function ticket_counter()
  {
    return $this->hasMany('App\TicketBooking','counter_id');
  }
}
