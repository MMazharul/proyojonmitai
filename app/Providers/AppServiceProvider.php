<?php

namespace App\Providers;

use App\GeneralSettings;
use App\Language;
use App\Menu;
use App\TripLocation;
use App\Social;
use App\FleetType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $data['lan'] = Language::all();
        $data['basic'] = GeneralSettings::first();
        $data['social'] = Social::all();
        $from = TripLocation::where('status', 1)->orderBy('name', 'asc')->get();

        $tripFrom = [];
        foreach ($from as $value) {
            $tripFrom[] = $value->name;
        }
        $fleetType = FleetType::where('status', 1)->orderBy('name', 'asc')->get();
        $type = [];
        foreach ($fleetType as $value) {
            $type[] = $value->name;
        }
        $data['tripFrom']=$tripFrom;
      
        view::share($data);

        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
