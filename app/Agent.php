<?php

namespace App;



use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Authenticatable
{
    protected $guarded = [];

    protected $table = "agents";

    protected $appends = ['full_name'];

    public function counter()
    {
        return $this->belongsTo('App\Counter','counter_id','id');
    }

    public function tickets()
    {
      return $this->hasMany('App\TicketBooking','agent_id');
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name." ".$this->last_name)." (".$this->username.")";
    }
}
