<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Counter;
use App\TripLocation;
use App\TripRoute;


class CounterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['page_title'] = "All Counter";
      $data['counters'] = Counter::with('location')->where('status',1)->latest()->paginate(10);
      return view('admin.counter.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = "Create Counter";

        $data['locations'] = TripLocation::where('status',1)->pluck('name','id');

        return view('admin/counter/create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();
      $request->validate([
          'counter_name' => 'required',
          'location_id' => 'required',
          'address' => 'required',
      ]);
      $data['status'] = $request->status == 'on' ? '1' : '0';
      Counter::create($data);
      $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
      return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = "Edit Counter";
        $data['locations'] = TripLocation::where('status',1)->pluck('name','id');
        $data['editData'] = Counter::findOrFail($id);
        return view('admin.counter.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $counterId = Counter::findOrFail($id);
        $request->validate([
            'counter_name' => 'required',
            'location_id' => 'required',
            'address' => 'required',
        ]);
        $data = $request->all();
        $data['status'] = $request->status == 'on' ? '1' : '0';
        $counterId->update($data);
        $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function findCounter(Request $request)
    {
      $route_id = $request->trip_route_id;

      $data = TripRoute::where('status',1)->where('id',$route_id)->first();
      $end_point = $data['end_point'];
      $start_point = $data['start_point'];

      $data['from_counter'] = Counter::where('location_id',$start_point)->get();
      $data['to_counter'] = Counter::where('location_id',$end_point)->get();

      return response()->json($data);


    }
}
