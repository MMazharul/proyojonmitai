<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class BaseController extends Controller
{
  public function create()
{
  try {
    

      // start the backup process
      Artisan::call('backup:mysql-dump');
      $output = Artisan::output();
        echo json_encode("success");
      // log the results
      //Log::info("Backpack\BackupManager -- new backup started from admin interface \r\n" . $output);
      // return the results as a response to the ajax call
      
     // return redirect()->back();
  } catch (Exception $e) {
      echo json_encode("fail");
    //  Flash::error($e->getMessage());

      //return redirect()->back();
  }
}

}
