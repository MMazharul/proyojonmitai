<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Counter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "All Agents";
        $data['agents'] = Agent::latest()->paginate(20);
        return view('admin.agent.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['counters'] = Counter::where('status',1)->pluck('counter_name','id');
        $data['page_title'] = "Create Agent";
        return view('admin.agent.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'counter_id' => 'required',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'phone' => 'required|max:50',
            'password' => 'required|min:5',
            'company_name' => 'required|max:50',
            'username' => 'required|max:50|alpha_dash|unique:agents,username',
            'email' => 'required|max:50|unique:agents',
            'picture' => 'nullable | mimes:jpeg,jpg,png | max:1024',
            'pic_document' => 'nullable | mimes:jpeg,jpg,png | max:2048'
        ]);




        $data['counter_id'] =  $request->counter_id;
        $data['city'] =  $request->city;
        $data['company_name'] =  $request->company_name;
        $data['country'] =  $request->country;
        $data['email'] =  strtolower(trim($request->email));
        $data['first_name'] =  $request->first_name;
        $data['last_name'] =  $request->last_name;
        $data['phone'] =  $request->phone;
        $data['permanent_address'] =  $request->permanent_address;
        $data['present_address'] =  $request->present_address;
        $data['username'] =  strtolower(trim($request->username));
        $data['status'] =  $request->status == 'on' ? '1' : '0';
        $data['zip_code'] =  $request->zip_code;
        $data['password'] =bcrypt($request->password);

        if($request->hasFile('picture')){
            $image = $request->file('picture');
            $filename = 'agent_'.time().'.jpg';
            $location = 'assets/images/agent/' . $filename;
            Image::make($image)->resize(800,800)->save($location);
            $data['picture'] = $filename;
        }

        if($request->hasFile('pic_document')){
            $image = $request->file('pic_document');
            $filename = 'agent_doc_'.time().'.jpg';
            $location = 'assets/images/agent/' . $filename;
            Image::make($image)->save($location);
            $data['pic_document'] = $filename;
        }

        Agent::create($data);

        $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
        return back()->with($notification);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Agent::findOrFail($id);
        $page_title = "Agent";
        return view('admin.agent.show', compact('page_title','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $counters = Counter::where('status',1)->pluck('counter_name','id');
        $data = Agent::findOrFail($id);
        $page_title = "Edit Agent";
        return view('admin.agent.edit', compact('page_title','data','counters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Agent::findOrFail($id);

        $request->validate([
            'counter_id' => 'required',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'phone' => 'required|max:50',
            'password' => 'nullable|min:5',
            'company_name' => 'required|max:50',
            'username' => 'required|max:50|alpha_dash|unique:agents,username,'.$data->id,
            'email' => 'required|max:50|unique:agents,email,'.$data->id,
            'picture' => 'nullable | mimes:jpeg,jpg,png | max:1024',
            'pic_document' => 'nullable | mimes:jpeg,jpg,png | max:2048'
        ]);


        $in['counter_id'] =  $request->counter_id;
        $in['city'] =  $request->city;
        $in['company_name'] =  $request->company_name;
        $in['country'] =  $request->country;
        $in['email'] =  strtolower(trim($request->email));
        $in['first_name'] =  $request->first_name;
        $in['last_name'] =  $request->last_name;
        $in['phone'] =  $request->phone;
        $in['permanent_address'] =  $request->permanent_address;
        $in['present_address'] =  $request->present_address;
        $in['username'] =  strtolower(trim($request->username));
        $in['status'] =  $request->status == 'on' ? '1' : '0';
        $in['zip_code'] =  $request->zip_code;
        $in['password'] =bcrypt($request->password);

        if($request->hasFile('picture')){
            $image = $request->file('picture');
            $filename = 'agent_'.time().'.jpg';
            $location = 'assets/images/agent/' . $filename;
            Image::make($image)->resize(800,800)->save($location);

            $path = './assets/images/agent/';
            File::delete($path.$data->picture);

            $in['picture'] = $filename;
        }

        if($request->hasFile('pic_document')){
            $image = $request->file('pic_document');
            $filename = 'agent_doc_'.time().'.jpg';
            $location = 'assets/images/agent/' . $filename;
            Image::make($image)->save($location);
            $path = './assets/images/agent/';
            File::delete($path.$data->pic_document);
            $in['pic_document'] = $filename;
        }

        $data->fill($in)->save();

        $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
        return back()->with($notification);




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
