<?php

namespace App\Http\Controllers;

use App\FleetRegistration;
use App\TripAssign;
use App\TripLocation;
use App\TripRoute;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use File;
use DB;
class TripManageController extends Controller
{
    /*
     * Trip Location
     */
    public function location()
    {
        $data['page_title'] = "Trip/Location";
        $data['location'] = TripLocation::orderBy('name','asc')->paginate(20);
        return view('admin.trip.location.index', $data);
    }
    public function locationCreate()
    {
        $data['page_title'] = "Trip/Location";
        return view('admin.trip.location.create', $data);
    }
    public function locationStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'nullable | mimes:jpeg,jpg | max:1000'
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = 'location_'.time().'.jpg';
            $location = 'assets/images/trip/' . $filename;
            Image::make($image)->resize(800,800)->save($location);
            $in['image'] = $filename;
        }
        $in['name'] =  $request->name;
        $in['description'] =  $request->description;
        $in['google_map'] =  $request->google_map;
        $in['status'] =  $request->status == 'on' ? 1 : 0;
        $res = TripLocation::create($in);

        if ($res) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
            return back()->with($notification);
        } else {
            $notification = array('message' => 'Something Error', 'alert-type' => 'error');
            return back()->with($notification);
        }
    }

    public function locationEdit($id)
    {
        $data['location'] =TripLocation::findOrFail($id);
        $data['page_title'] = "Edit Trip/Location";
        return view('admin.trip.location.edit', $data);
    }

    public function locationUpdate(Request $request, $id)
    {
        $data = TripLocation::findOrFail($id);

        $in['name'] =  $request->name;
        $in['description'] =  $request->description;
        $in['google_map'] =  $request->google_map;
        $in['status'] =  $request->status == 'on' ? 1 : 0;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = 'location_'.time().'.jpg';
            $location = 'assets/images/trip/' . $filename;
            Image::make($image)->resize(800,800)->save($location);

            $path = './assets/images/trip/';
            File::delete($path.$data->image);
            $in['image'] = $filename;
        }

        $res = $data->fill($in)->save();

        if ($res) {
            $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
            return back()->with($notification);
        } else {
            $notification = array('message' => 'Something Error', 'alert-type' => 'error');
            return back()->with($notification);
        }
    }

    /*
     * Trip Route
     */

    public function route()
    {
        $data['page_title'] = "Trip/Route";
        $data['location'] = TripRoute::orderBy('name','asc')->paginate(20);
        return view('admin.trip.route.index', $data);
    }
    public function routeCreate()
    {
        $data['page_title'] = "Trip/Route";
        $data['tripLocation'] = TripLocation::where('status',1)->orderBy('name','asc')->get();
        return view('admin.trip.route.create', $data);
    }
    public function routeStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'start_point' => 'required',
            'end_point' => 'required',
            'approximate_time' => 'required',
            'distance' => 'required',
        ]);

        $start_point =  TripLocation::find($request->start_point);
        $end_point =  TripLocation::find($request->end_point);

        $in['name'] =  $request->name;
        $in['start_point'] =  $request->start_point;
        $in['start_point_name'] =  $start_point->name;

        $in['end_point'] =  $request->end_point;
        $in['end_point_name'] =  $end_point->name;

        $in['distance'] =  $request->distance;
        $in['approximate_time'] =  $request->approximate_time;
        $in['stoppage'] =  $request->stoppage;
        $in['status'] =  $request->status == 'on' ? 1 : 0;
        $res = TripRoute::create($in);

        if ($res) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
            return back()->with($notification);
        } else {
            $notification = array('message' => 'Something Error', 'alert-type' => 'error');
            return back()->with($notification);
        }
    }

    public function routeEdit($id)
    {
        $data['page_title'] = "Edit Trip/Route";
        $data['route'] = TripRoute::findOrFail($id);
        $data['tripLocation'] = TripLocation::where('status',1)->orderBy('name','asc')->get();
        return view('admin.trip.route.edit', $data);
    }

    public function routeUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'start_point' => 'required',
            'end_point' => 'required',
            'approximate_time' => 'required',
            'distance' => 'required',
        ]);

        $data = TripRoute::findOrFail($id);

        $start_point =  TripLocation::find($request->start_point);
        $end_point =  TripLocation::find($request->end_point);


        $in['name'] =  $request->name;
        $in['start_point'] =  $request->start_point;
        $in['start_point_name'] =  $start_point->name;

        $in['end_point'] =  $request->end_point;

        $in['end_point_name'] =  $end_point->name;

        $in['distance'] =  $request->distance;
        $in['approximate_time'] =  $request->approximate_time;
        $in['stoppage'] =  $request->stoppage;
        $in['status'] =  $request->status == 'on' ? 1 : 0;

        $res = $data->fill($in)->save();

        if ($res) {
            $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
            return back()->with($notification);
        } else {
            $notification = array('message' => 'Something Error', 'alert-type' => 'error');
            return back()->with($notification);
        }
    }

    /*
     * Trip route assign
     */
    public function tripAssign()
    {

        $data['page_title'] = "Trip/Assign";
        $data['tripAssign'] = TripAssign::orderBy('start_date','asc')->where('end_date', '>', Carbon::now())->paginate(20);
        // echo "<pre>";
        // echo json_encode($data);
        // die;
        return view('admin.trip.assign.index', $data);
    }

      public function tripAllAssign()
    {
        $data['page_title'] = "All Trip/Assign";
        $data['tripAssign'] = TripAssign::orderBy('start_date','asc')
        ->where('end_date','<',Carbon::now())
        ->paginate(10);
        return view('admin.trip.assign.all_assign', $data);
    }

    public function tripViewAdmin($id)
    {
      $data['ticket_sales'] = DB::table('ticket_bookings')
                          ->join('agents', 'ticket_bookings.agent_id', '=', 'agents.id')
                          ->select('ticket_bookings.fleet_registration_id','ticket_bookings.passenger_name','ticket_bookings.email','ticket_bookings.phone','ticket_bookings.seat_number','ticket_bookings.total_fare','agents.username')
                          ->where('payment_status',1)
                          ->where('cancel_req',0)
                          ->Where('id_no',$id)
                          ->get();
      $data['page_title']="Chalan sheet";
      $data['id']=$id;
      $fleet_registration_id = array_column(json_decode($data['ticket_sales'],true),'fleet_registration_id');
      if($fleet_registration_id)
      {
        $data['driverInfo'] = DB::table('ride_infos')->where('bus_id', $fleet_registration_id[0])->first();
      }
      return view('admin.trip.assign.view', $data);
    }
    public function tripPrintView($id)
    {
      $data['passengers'] = DB::table('ticket_bookings')
                          ->join('agents', 'ticket_bookings.agent_id', '=', 'agents.id')
                          ->select('ticket_bookings.fleet_registration_id','ticket_bookings.passenger_name','ticket_bookings.email','ticket_bookings.phone','ticket_bookings.seat_number','ticket_bookings.total_fare','agents.username')
                          ->where('payment_status',1)
                          ->where('cancel_req',0)
                          ->Where('id_no',$id)
                          ->get();
      $data['page_title']="Chalan sheet";
      $fleet_registration_id = array_column(json_decode($data['passengers'],true),'fleet_registration_id');
      if($fleet_registration_id)
      {
        $data['driverInfo'] = DB::table('ride_infos')->where('bus_id', $fleet_registration_id[0])->first();
      }

      return view('admin.report.print_pasenger_info', $data);
    }


    public function tripAssignCreate()
    {
        $data['page_title'] = "Trip/Assign";
        $data['fleet_registration'] = FleetRegistration::where('status',1)->get();
        $data['tripRoute'] = TripRoute::where('status',1)->orderBy('id','asc')->get();
        return view('admin.trip.assign.create', $data);
    }
    public function tripAssignStore(Request $request)
    {
          $request->validate([
            'fleet_registration_id' => 'required',
            'trip_route_id' => 'required',
            'start_point' => 'required',
            'end_point' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

       $tripRoute = TripRoute::where('id',$request->trip_route_id)->first();
       $start_point = $tripRoute->start_point;
       $end_point = $tripRoute->end_point;
       $start_point_short = TripLocation::where('id',$start_point)->first();
       $end_point_short = TripLocation::where('id',$end_point)->first();
       $reg_id = FleetRegistration::where('id',$request->fleet_registration_id)->first();
        $in['id_no'] =  time();
        $in['fleet_registration_id'] =  $request->fleet_registration_id;
        $in['trip_route_id'] =  $request->trip_route_id;
        $in['start_point'] =  $request->start_point;
        $in['end_point'] =  $request->end_point;
        $in['coach_no'] =  $reg_id->reg_no.'-'.$start_point_short->short_name.'-'.$end_point_short->short_name;
        $in['start_date'] =  Carbon::parse($request->start_date);
        $in['end_date'] =  Carbon::parse($request->end_date);

        $in['status'] =  $request->status == 'on' ? 1 : 0;
        $res = TripAssign::create($in);

        if ($res) {
            $notification = array('message' => 'Created Successfully!', 'alert-type' => 'success');
            return back()->with($notification);
        } else {
            $notification = array('message' => 'Something Error', 'alert-type' => 'error');
            return back()->with($notification);
        }
    }

    public function tripAssignEdit($id)
    {
        $data['page_title'] = "Edit Trip/Assign";
        $data['tripAssign'] = TripAssign::findOrFail($id);
        $data['fleet_registration'] = FleetRegistration::where('status',1)->get();
        $data['tripRoute'] = TripRoute::where('status',1)->orderBy('id','asc')->get();
        return view('admin.trip.assign.edit', $data);
    }

    public function tripAssignUpdate(Request $request, $id)
    {
           $request->validate([
            'fleet_registration_id' => 'required',
            'trip_route_id' => 'required',
            'start_point' => 'required',
            'end_point' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $tripRoute = TripRoute::where('id',$request->trip_route_id)->first();
        $start_point = $tripRoute->start_point;
        $end_point = $tripRoute->end_point;
        $start_point_short = TripLocation::where('id',$start_point)->first();
        $end_point_short = TripLocation::where('id',$end_point)->first();
        $reg_id = FleetRegistration::where('id',$request->fleet_registration_id)->first();


        $data = TripAssign::findOrFail($id);
        $in['fleet_registration_id'] =  $request->fleet_registration_id;
        $in['trip_route_id'] =  $request->trip_route_id;
        $in['start_point'] =  $request->start_point;
        $in['end_point'] =  $request->end_point;
        $in['coach_no'] =  $reg_id->reg_no.'-'.$start_point_short->short_name.'-'.$end_point_short->short_name;
        $in['start_date'] =  Carbon::parse($request->start_date);
        $in['end_date'] =  Carbon::parse($request->end_date);

        $in['status'] =  $request->status == 'on' ? 1 : 0;

        $res = $data->fill($in)->save();

        if ($res) {
            $notification = array('message' => 'Updated Successfully!', 'alert-type' => 'success');
            return back()->with($notification);
        } else {
            $notification = array('message' => 'Something Error', 'alert-type' => 'error');
            return back()->with($notification);
        }
    }
    public function tripAssignDelete($id)
    {
        TripAssign::where('id',$id)->delete();
        $notification = array('message' => 'Delete Successfully!!', 'alert-type' => 'success');
        return back()->with($notification);
    }


    public function tripClose()
    {
        $data['page_title'] = "Trip Close";
        $data['tripAssign'] = TripAssign::orderBy('start_date','asc')->where('end_date', '<', Carbon::now())->where('status',2)->paginate(20);
        return view('admin.trip.close.index', $data);
    }



}
