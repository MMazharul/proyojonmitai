<?php

namespace App\Http\Controllers;

use App\GeneralSettings;
use Illuminate\Http\Request;
use App\WhyUs;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use File;
class WhyUsController extends Controller
{
    public function updateGnl(Request $request)
    {
        $basic = GeneralSettings::first();
        $basic->why_us_h =  $request->why_us_h;
        $basic->why_us_p =  $request->why_us_p;
        $basic->save();
        return back()->with('success', 'Updated Successfully!');
    }
    public function index()
    {
        $data['page_title'] = "Why Us";
        $data['posts'] = WhyUs::latest()->paginate(20);
        return view('admin.whyus.index', $data);
    }

    public function create()
    {
        $data['page_title'] = 'Why Us';
        return view('admin.whyus.add', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'icon' => 'required',
        ],
            [
                'name.required' => 'Title must not be empty',
            ]
        );

        $in = Input::except('_token');
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = 'destination_'.time().'.jpg';
            $location = 'assets/images/tour/' . $filename;
            Image::make($image)->resize(380,340)->save($location);
            $in['image'] = $filename;
        }
        $res = WhyUs::create($in);
        if ($res) {
            return back()->with('success', 'Saved Successfully!');
        } else {
            return back()->with('alert', 'Problem With Updating Plan');
        }

    }

    public function edit($id)
    {
        $data['page_title'] = 'Why Us';
        $data['post'] = WhyUs::find($id);
        return view('admin.whyus.edit', $data);
    }
    public function updatePost(Request $request)
    {

        $data = WhyUs::find($request->id);
        $request->validate([
            'name' => 'required',
            'icon' => 'required',
        ],
            [
                'name.required' => 'Title must not be empty',
            ]
        );


        $in = Input::except('_token');
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = 'destination_'.time().'.jpg';
            $location = 'assets/images/tour/' . $filename;
            Image::make($image)->resize(380,340)->save($location);
            $path = './assets/images/tour/';
            File::delete($path.$data->image);
            $in['image'] = $filename;
        }
        $res = $data->fill($in)->save();

        if ($res) {
            return back()->with('success', 'Updated Successfully!');
        } else {
            return back()->with('alert', 'Problem With Updating Plan');
        }
    }

    public function destroy(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        $data = WhyUs::findOrFail($request->id);
        $path = './assets/images/tour/';
        File::delete($path.$data->image);
        $res =  $data->delete();

        if ($res) {
            return back()->with('success', 'Delete Successfully!');
        } else {
            return back()->with('alert', 'Problem With Deleting Post');
        }
    }
}
