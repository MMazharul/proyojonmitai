<?php

namespace App\Http\Controllers;

use App\GeneralSettings;
use App\LogPdf;
use App\TicketBooking;
use App\TicketCancel;
use App\TicketPrice;
use App\TripAssign;
use App\TripLocation;
use App\TripRoute;
use App\Trx;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManageTripController extends Controller
{
    public function reqeustCancellation(){
        $data['page_title'] = "Ticket Cancel Request";
        $data['ticket_cancels'] = TicketCancel::latest()->paginate(25);
        return view('admin.pages.request-cancel',$data);
    }

    public function reqeustCancellationAction(Request $request)
    {
        $basic = GeneralSettings::first();
        if($request->sbtn == "yes")
        {
            $ticCancel =  TicketCancel::findOrFail($request->id);
            $ticCancel->amount = round($request->amount,$basic->decimal);
            $ticCancel->charge = round($request->charge,$basic->decimal);
            $ticCancel->status = 1;

            $ticBook = TicketBooking::findOrFail($ticCancel->ticket_booking_id);
            $ticBook->status  = -1; // trip cancel
            $ticBook->save();

            $user = User::findOrFail($ticCancel->user_id);
            $user->balance +=  round($request->amount,$basic->decimal);
            $user->save();

           $ticCancel->save();

           Trx::create([
               'user_id' => $user->id,
               'amount' => $ticCancel->amount,
               'main_amo' => $user->balance,
               'charge' => $ticCancel->charge,
               'type' => '+',
               'role' => 1,
               'title' => date('D d M Y h:i A', strtotime($ticBook->booking_date))." (".$ticBook->tripRoute->start_point_name."-".$ticBook->tripRoute->end_point_name.") Trip has been Cancelled <br> PNR: ".$ticBook->pnr,
               'trx' => 'TRX-'.rand(000000,999999).rand(000000,999999),
           ]);


            $msg = date('D d M Y h:i A', strtotime($ticBook->booking_date))." (".$ticBook->tripRoute->start_point_name."-".$ticBook->tripRoute->end_point_name.") Trip has been Cancelled <br> PNR: ".$ticBook->pnr;
            $msg .= "<br> ".round($ticCancel->amount, $basic->decimal) ." $basic->currency  added in your account";
            $msg .= " Your current balance ".round($user->balance, $basic->decimal) ." $basic->currency";
            send_email($user->email, $user->username, 'Ticket Cancel Request Approved', $msg);
            send_sms($user->phone, $msg);

           $notify = array('message' => 'Ticket request has been approved  successfully', 'alert-type' => 'success');
        }elseif($request->sbtn == "reject"){

            $ticCancel =  TicketCancel::findOrFail($request->id);
            $ticCancel->status = -1;

            $ticBook = TicketBooking::findOrFail($ticCancel->ticket_booking_id);

            $user = User::findOrFail($ticCancel->user_id);
            $ticCancel->save();

            $msg = date('D d M Y h:i A', strtotime($ticBook->booking_date))." (".$ticBook->tripRoute->start_point_name."-".$ticBook->tripRoute->end_point_name.") Trip  Cancelation has been rejected by Admin <br> PNR: ".$ticBook->pnr;
            send_email($user->email, $user->username, 'Ticket Cancel Request', $msg);
            send_sms($user->phone, $msg);

            $notify = array('message' => 'Ticket request has been rejected  successfully', 'alert-type' => 'success');
        }
        return back()->with($notify);
    }


    public function trip_cancel(Request $request)
    {

        $ticBook = new TicketBooking();
        $ticBook = $ticBook->where('pnr', $request->pnr)->where('cancel_req','!=',1)->first();

        if(!$ticBook){
            $notify = array('message' => 'Ticket already canceled ', 'alert-type' => 'danger');
            return back()->with($notify);
        }

        $ticBook->update([
            'status' => -1,
            'cancel_req' => 1,
            'cancel_endtime' => date("Y-m-d H:i:s")
        ]);

        $inputs = [];
        $inputs['user_id'] = Auth::user()->id;
        $inputs['ticket_booking_id'] = $ticBook->id;
        $inputs['amount'] = $ticBook->total_fare;
        $inputs['charge'] = $ticBook->commission;
        $inputs['status'] = 1;
        TicketCancel::create($inputs);

//        $phone = phone_number_format($ticBook->phone);
//        $msg = date('D d M Y h:i A', strtotime($ticBook->booking_date))." (".$ticBook->tripRoute->start_point_name."-".$ticBook->tripRoute->end_point_name.") PNR: ".$ticBook->pnr."Ticket request has been canceled  successfully. By Shadin Travels";
//        send_sms($phone, $msg);

        $notify = array('message' => 'Ticket request has been canceled  successfully', 'alert-type' => 'success');
        return back()->with($notify);
    }





}
